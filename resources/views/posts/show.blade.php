@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted">Likes: {{count($post->likes)}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			@if(Auth::user())
				@if(Auth::id() != $post->user_id)
					<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
						@method('PUT')
						@csrf
						@if($post->likes->contains("user_id", Auth::id()))
							<button type="submit" class="btn btn-danger">Unlike</button>
						@else
							<button type="submit" class="btn btn-success">Like</button>
						@endif
					</form>
				@endif	
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
				  Post a comment
				</button>			
			@endif
			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="commentModal" tabindex="-1" aria-labelledby="commentModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Write a Comment</h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body">
	        <form method="POST" action="/posts/{{$post->id}}/comment">
	          @csrf
	            <label for="content">Content:</label>
	            <textarea class="form-control" id="content" name="content" rows="3"></textarea>
	          </div>
	          <div class="modal-footer">
	            <button type="submit" class="btn btn-primary">Post comment</button>
	            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
	          </div>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- End of Modal -->

@endsection

